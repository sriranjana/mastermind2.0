et rec combinaison n a = if n = 0 then [[]]
        else let combinaison_aux = combinaison (n-1) a in List.concat (List.map (fun m -> List.map(fun s -> s@m) a) combinaison_aux);;
   
    let rec player_ans_to_computer code1 code2 =
        print_int ((fst (reponse code1 code2)));
        print_int ((snd (reponse code1 code2)));
       
        print_newline();
        print_string "Code à deviner par l'ordi: ";
        print_string (string_of_code code2);
        print_newline();
        print_string "Code proposé par l'ordi:" ;
        print_string (string_of_code code1);
        print_newline();
        print_string "Combien y a t'il de pions bien placés ? " ;
       
        print_newline();
        let a = read_int() in
         print_string "Combien y a t'il de pions mal placés ? " ;
        let b = read_int() in
        print_newline();
        print_string "--------------------------------------------------";   
        print_newline();
       
        let x = (a,b) in
        match x with
        |rep when (rep = reponse code1 code2)-> x;
        |_ -> print_string ("Vous n'avez pas bien saisie la réponse");
            player_ans_to_computer code1 code2;;
           
           
           
(*les 2 fonctions qui suivent permettent de verifier si le code séléctionné par l'ordi concorde avec la disposition des pions (bien ou mal placé) *)
    let rec verif_coresp_aux liste code1 rep = if reponse code1 liste = rep then true else false;;
           
    let rec verif_coresp liste_possible code1 code2 rep = match liste_possible with
        | h :: t when (verif_coresp_aux h code1 rep) -> h :: (verif_coresp t code1 code2 rep);
        | h :: t -> (verif_coresp t code1 code2 rep);       
        | _ -> [];;
       
       
(*le joueur choisi de répondre a l'ordi ou bien vérification auto des pions*)
    let player_choice_mode code1 code2 player = match player with
        |true -> player_ans_to_computer code1 code2;
        |false -> reponse code1 code2;;

