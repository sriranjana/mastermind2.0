let rec prendre_liste liste pos n=
			match liste with 
				|h::t when (pos = n) -> t;
			    |h::t -> h::prendre_liste t pos (n+1);
				| _ -> [];;
				
		let rec prendre_couleur liste couleur=
			match liste with
				|h::t when (h = couleur) -> t;
				|h::t -> h::prendre_couleur t couleur;
				| _ -> [];;
			
		let rec comparer_mauvais code1 code2 k (pionbon,pionmauvais) = 
			if k<(List.length code1) then
					match (List.nth code1 k) with 
						|m when (List.exists ((=) m) code2)-> comparer_mauvais (prendre_liste code1 k 0) (prendre_couleur code2 m) (k) (pionbon,pionmauvais+1 );
						|_ -> comparer_mauvais code1 code2 (k+1) (pionbon,pionmauvais)
			else (pionbon,pionmauvais);;
	
		let rec comparer_bon code1 code2 k (pionbon,pionmauvais) = 
			if k < (List.length code1) then
					match (List.nth code1 k) with 
						|m when (m = List.nth code2 k) -> comparer_bon (prendre_liste code1 k 0) (prendre_liste code2 k 0) (k) (pionbon,pionmauvais );
						|_ ->  comparer_bon (code1) (code2) (k+1) (pionbon,pionmauvais)
			else comparer_mauvais code1 code2 0 (pionbon,pionmauvais);;


		let rep  code1 code2 = comparer_bon code1 code2 0 (0,0);; 
