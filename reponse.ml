module Code :
	sig
		val tous_aux : int -> t list
		val tous : t list
		val toutes_reponses : (int*int) list
		end=
		type pion = Rouge | Vert | Orange | Violet | Bleu | Jaune | Vide
		type t = pion list
		let rec code_of_string code = 
			match code with
				|h::t -> (pion_of_string h)::(code_of_string  t)
				|_ -> [Vide];;
				
		let rec  tous_aux n =
				let rec creation code = match code with
												|[]->[]
												|h::t -> ((Rouge::h)::((Vert::h)::((Orange::h)::((Violet::h)::((Bleu::h)::((Jaune::h)::(creation t))))))) in match n with
																																									  |1 ->[[Rouge];[Vert];[Orange];[Violet];[Bleu]]
																																									  |_->creation (tous_aux (n-1));;
		let tous = tous_aux  4;;																																							  
		let toutes_reponses = [(4,0);(3,0);(2,0);(2,1);(2,2);(1,0);(1,1);(1,2);(1,3);(0,0);(0,1);(0,2);(0,3);(0,4)];;
		
		end;;
