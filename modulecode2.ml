module Code :
	sig
		type pion = Rouge | Vert | Orange | Violet | Bleu | Jaune | Vide
		type t = pion list
		val nombre_pions: int 
		val couleurs_possibles : t
		val string_of_pion : pion -> string
		val string_of_code : t -> string
		val pion_of_string : string -> pion
		val code_of_string : string list -> t list
		val tous_aux : int -> t list
		val tous : t list
		struct
		type pion = Rouge | Vert | Orange | Violet | Bleu | Jaune | Vide
		type t = pion list
		let nombre_pions = 4;;
		let couleurs_possibles= [Rouge ; Vert ; Orange ; Violet ; Bleu ; Jaune];;
				
		let string_of_pion pion = 
			match pion with 
				|Rouge -> "Rouge;"
				|Vert-> "Vert;"
				|Orange-> "Orange;"
				|Violet-> "Violet;"
				|Bleu -> "Bleu;"
				|Jaune-> "Jaune;"
				|Vide -> "";;
		
		let rec string_of_code code = 
			match code with
				|[] -> ""
				|h::t -> (string_of_pion h)^(string_of_code t);;
				
		let pion_of_string code =
			match code with
				|"Rouge" -> Rouge
				|"Vert"-> Vert
				|"Orange"-> Orange
				|"Violet"-> Violet
				|"Bleu" -> Bleu;
				|"Jaune"-> Jaune;
				|_-> Vide;;
		
		let rec code_of_string code = 
			match code with
				|h::t -> (pion_of_string h)::(code_of_string  t)
				|_ -> [Vide];;
		
		end;;
